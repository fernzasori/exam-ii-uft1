﻿iRowCount = Datatable.getSheet("TC04_TESTWEB [TC04_TESTWEB]").getRowCount
username = Trim((DataTable("Username","TC04_TESTWEB [TC04_TESTWEB]")))
password = Trim((DataTable("Password","TC04_TESTWEB [TC04_TESTWEB]")))
note = Trim((DataTable("Note","TC04_TESTWEB [TC04_TESTWEB]")))

Call FW_TransactionStart("TC04 TESTWEB - Open web DEMOSTORE.x-CART")
Call FW_OpenWebBrowser("https://demostore.x-cart.com/","CHROME")
Call FW_TransactionEnd("TC04 TESTWEB - Open web DEMOSTORE.x-CART")

Call FW_TransactionStart("TC04 TESTWEB - Click button for login")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Sign in / sign up")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Sign in / sign up").Click
Call FW_TransactionEnd("TC04 TESTWEB - Click button for login")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Sign in / sign up").Click

Call FW_TransactionStart("TC04 TESTWEB - Input Username")
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","login", username)
Call FW_TransactionEnd("TC04 TESTWEB - Input Username")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebEdit("login").Set "gggg"


Call FW_TransactionStart("TC04 TESTWEB - Input Password")
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","password", password)
Call FW_TransactionEnd("TC04 TESTWEB - Input Password")


Call FW_TransactionStart("TC04 TESTWEB -  Click Sign in")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Sign in")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebButton("Sign in").Click
Call FW_TransactionEnd("TC04 TESTWEB - Click Sign in")


Call FW_TransactionStart("TC04 TESTWEB - Select Manu New!")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","New!")
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu New!")


Call FW_TransactionStart("TC04 TESTWEB - Select Manu Bestsellers")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Bestsellers")
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu Bestsellers")

Browser("X-Cart Demo store company").Page("X-Cart Demo store company_2").WebElement("Healthy Food & Snacks").Click


'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("Fashion").Click
Call FW_TransactionStart("TC04 TESTWEB - Select Manu Fashion")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Fashion")
Call FW_TransactionEnd("TC04 TESTWEB - Select Manu Fashion")
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Fashion").Click


Call FW_TransactionStart("TC04 TESTWEB - Select Shoes Category")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Shoes")
Call FW_TransactionEnd("TC04 TESTWEB - Select Shoes Category")

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").WebElement("Shoes").Click


Call FW_TransactionStart("TC04 TESTWEB - Select Products White Lace-Up Mesh Trainers")
Call FW_Image("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","White Lace-Up Mesh Trainers")
Call FW_TransactionEnd("TC04 TESTWEB - Select Products White Lace-Up Mesh Trainers")

Call FW_TransactionStart("TC04 TESTWEB -  Click Add to cart")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Add to cart")
Call FW_TransactionEnd("TC04 TESTWEB - Click Add to cart")

Call FW_TransactionStart("TC04 TESTWEB - Click View cart")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","View cart")
Call FW_TransactionEnd("TC04 TESTWEB - Click  View cart")

Call FW_TransactionStart("TC04 TESTWEB - Click Go to checkout")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Go to checkout")
Call FW_TransactionEnd("TC04 TESTWEB - Click Go to checkout")

Call FW_TransactionStart("TC04 TESTWEB - Input Note")
Call FW_WebEdit("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","notes",note)
Call FW_TransactionEnd("TC04 TESTWEB - Input Note")

Call FW_TransactionStart("TC04 TESTWEB - Click Proceed to payment")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Proceed to payment")
Call FW_TransactionEnd("TC04 TESTWEB - Click Proceed to payment") 

Call FW_TransactionStart("TC04 TESTWEB - Click Place order")
Call FW_WebButton2("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","Place order")
Call FW_TransactionEnd("TC04 TESTWEB - Click Place order") 


Call FW_TransactionStart("TC04 TESTWEB - Check text Thank you for your order")
Call FW_WebCheckText("TC04 TESTWEB","X-Cart Demo store company","X-Cart Demo store company","page-title")
Call FW_TransactionEnd("TC04 TESTWEB - Check text Thank you for your order") 
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").WebElement("page-title").Click

Call FW_TransactionStart("TC04 TESTWEB - Click My account for logout")
Call FW_WebElement("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","My account")
Call FW_TransactionEnd("TC04 TESTWEB - Click My account for logout") 


Call FW_TransactionStart("TC04 TESTWEB - Click Log out")
Call FW_Link("TC04 TESTWEB", "X-Cart Demo store company","X-Cart Demo store company","Log out")
Call FW_TransactionEnd("TC04 TESTWEB - Click Log out")

Call FW_TransactionStart("TC04 TESTWEB - Close Web DEMOSTORE.x-CART")
Call FW_CloseWebBrowser("CHROME")
Call FW_TransactionEnd("TC04 TESTWEB - Close Web DEMOSTORE.x-CART")



'Browser("X-Cart Demo store company").Page("X-Cart Demo store company_2").WebElement("Hot deals").Click

'Browser("X-Cart Demo store company").Page("X-Cart Demo store company_2").Link("Bestsellers").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").Link("Fashion").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_2").Link("Shoes").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_3").Image("White Lace-Up Mesh Trainers").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_4").WebButton("Add to cart").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_4").Link("View cart").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Go to checkout").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebEdit("notes").Set "test"

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Proceed to payment").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebButton("Place order").Click

'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company_5").WebElement("page-title").Click



'Browser("X-Cart Demo store company_2").Page("X-Cart Demo store company").Link("Fashion").Click



'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Fashion").Click
'Browser("X-Cart Demo store company").Page("X-Cart Demo store company").Link("Shoes").Click

